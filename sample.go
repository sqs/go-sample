package sample

// Foo is a cool function!
func Foo() (*Bar, *Baz, error) {
	return &Bar{}, &Baz{}, nil
}

// Bar is a neat struct.
type Bar struct {
	// Xyz is a fun field.
	Xyz Baz
}

// Baz is a cool struct.
type Baz struct {
	// Abc is the best field.
	Abc string
}

var (
	a Bar
	b Baz
	c = Bar{Xyz: b}
)
